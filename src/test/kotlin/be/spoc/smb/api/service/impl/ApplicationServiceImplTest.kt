package be.spoc.smb.api.service.impl

import be.spoc.smb.api.domain.Application
import be.spoc.smb.api.repository.ApplicationRepository
import be.spoc.smb.api.service.ApplicationService
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.util.Optional
import java.util.UUID
import javax.persistence.EntityNotFoundException

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [ApplicationServiceImpl::class])
class ApplicationServiceImplTest {

    lateinit var id: UUID
    lateinit var application: Application

    @Autowired
    lateinit var service: ApplicationService

    @MockBean
    lateinit var repository: ApplicationRepository

    @Before
    fun setup() {
        id = UUID.randomUUID()
        application = Application()
        `when`(repository.findById(id)).thenReturn(Optional.of(application))
    }


    @Test
    fun findById_found() {
        val actual = service.findById(id)
        assertThat(actual).isSameAs(application)
    }

    @Test(expected = EntityNotFoundException::class)
    fun findById_notFound() {
        service.findById(UUID.randomUUID())
    }

    @Test
    fun save() {
        service.save(application)
        verify(repository).save(application)
    }
}