package be.spoc.smb.api.util
import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.lang3.RandomUtils

import java.math.BigDecimal
import java.time.LocalDate

object RandomValueUtil {
    @JvmOverloads
    fun randomString(length: Int = 10): String {
        return RandomStringUtils.randomAlphabetic(length)
    }

    fun randomEmail(): String {
        return randomString(15) + "@mail.com"
    }

    @JvmOverloads
    fun randomInt(min: Int = 0, max: Int = Integer.MAX_VALUE): Int {
        return RandomUtils.nextInt(min, max)
    }

    fun randomDecimal(): BigDecimal {
        return BigDecimal.valueOf(RandomUtils.nextDouble(0.0, java.lang.Double.MAX_VALUE))
    }

    @JvmOverloads
    fun randomLong(min: Long = 0, max: Long = java.lang.Long.MAX_VALUE): Long {
        return RandomUtils.nextLong(min, max)
    }

    fun randomLocalDate(): LocalDate {
        return LocalDate.now().plusDays(randomLong(0, 1024))
    }
}