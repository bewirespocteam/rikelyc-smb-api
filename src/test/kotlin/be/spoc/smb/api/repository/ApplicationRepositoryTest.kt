package be.spoc.smb.api.repository

import be.spoc.smb.api.ApplicationFixture
import be.spoc.smb.api.domain.Application
import be.spoc.smb.api.domain.ApplicationStatus
import be.spoc.smb.api.domain.Capacity
import be.spoc.smb.api.domain.MaritalStatus
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.junit4.SpringRunner
import java.time.LocalDateTime
import java.util.Optional
import javax.persistence.PersistenceException

@RunWith(SpringRunner::class)
@DataJpaTest
class ApplicationRepositoryTest {
    @Autowired
    lateinit var entityManager: TestEntityManager
    @Autowired
    lateinit var applicationRepository: ApplicationRepository

    val application = ApplicationFixture.mutable()

    @Test
    fun completeEntity() {
        persistAndClear()
        val result = assertEntityFound()
        assertThat(result.get().contactPerson!!.person!!.address!!.city).isEqualTo("city")
        assertThat(result.get().contactPerson!!.person!!.maritalStatus!!.significantOtherName)
                .isEqualTo("so-name")
        assertThat(result.get().contactPerson!!.person!!.maritalStatus!!.status)
                .isEqualTo(MaritalStatus.Status.MARRIED)
    }

    @Test
    fun withoutMaritialStatus() {
        application.contactPerson!!.person!!.maritalStatus = null
        persistAndClear()
        val result = assertEntityFound()
        assertThat(result.get().contactPerson!!.person!!.maritalStatus).isNull()
    }

    @Test
    fun withoutAddress() {
        application.contactPerson!!.person!!.address = null
        persistAndClear()
        val result = assertEntityFound()
        assertThat(result.get().contactPerson!!.person!!.address).isNull()
    }

    @Test(expected = PersistenceException::class)
    fun withoutPerson_fails() {
        application.contactPerson!!.person = null
        persistAndClear()
    }

    @Test(expected = PersistenceException::class)
    fun withoutContactPerson_fails() {
        application.contactPerson = null
        persistAndClear()
    }

    @Test(expected = PersistenceException::class)
    fun marriedButSignificantOtherNotPresent_fails() {
        application.contactPerson!!.person!!.maritalStatus!!.significantOtherName = null
        persistAndClear()
    }

    @Test(expected = PersistenceException::class)
    fun notNullCheck_ContactPersonCapacity() {
        application.contactPerson!!.capacity = null
        persistAndClear()
    }

    @Test(expected = PersistenceException::class)
    fun notNullCheck_nameOfFirm() {
        application.contactPerson!!.nameOfFirm = null
        persistAndClear()
    }

    @Test
    fun notNullCheck_ContactPersonNameOfFirm() {
        application.contactPerson!!.capacity = Capacity.FOUNDER
        application.contactPerson!!.nameOfFirm = null
        persistAndClear()
    }

    private fun assertEntityFound(): Optional<Application> {
        val result = applicationRepository.findById(application.id!!)
        assertThat(result.get().id).isNotNull()
        assertThat(result.get().createdOn).isAfter(LocalDateTime.now().minusSeconds(5L))
        assertThat(result.get().status).isSameAs(ApplicationStatus.INITIATED)
        assertThat(result.get().contactPerson!!.nameOfFirm).isEqualTo("firm")
        assertThat(result.get().contactPerson!!.person!!.email).isEqualTo("email")
        return result
    }

    private fun persistAndClear() {
        applicationRepository.save(application)
        entityManager.flush()
        entityManager.clear()
    }
}