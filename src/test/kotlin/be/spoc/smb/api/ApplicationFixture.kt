package be.spoc.smb.api

import be.spoc.smb.api.domain.Address
import be.spoc.smb.api.domain.Application
import be.spoc.smb.api.domain.Capacity
import be.spoc.smb.api.domain.ContactPerson
import be.spoc.smb.api.domain.MaritalStatus
import be.spoc.smb.api.domain.Person
import java.time.LocalDateTime

object ApplicationFixture {
    fun mutable(): Application {
        return Application(
                contactPerson = ContactPerson(
                        capacity = Capacity.PROFESSIONAL,
                        nameOfFirm = "firm",
                        person = Person(
                                dateOfBirth = LocalDateTime.now(),
                                email = "email",
                                name = "name",
                                placeOfBirth = "placeOfBirth",
                                surname = "surname",
                                telephoneNumber = "telnr",
                                address = Address(
                                        city = "city",
                                        countryCode = "BE",
                                        houseNumber = "123",
                                        postCode = "B-1234",
                                        street = "street"
                                ),
                                maritalStatus = MaritalStatus(
                                        maritalContract = MaritalStatus.MaritalContract
                                                .COMMUNITY_OF_GOODS_CONTRACT,
                                        significantOtherName = "so-name",
                                        significantOtherSurname = "so-surname",
                                        status = MaritalStatus.Status.MARRIED
                                )
                        ))

        )
    }
}