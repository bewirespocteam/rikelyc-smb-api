package be.spoc.smb.api.repository

import be.spoc.smb.api.domain.Application
import org.springframework.data.jpa.repository.JpaRepository
import java.util.UUID


interface ApplicationRepository : JpaRepository<Application, UUID> {
}