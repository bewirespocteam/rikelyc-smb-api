package be.spoc.smb.api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SmbApiApplication

fun main(args: Array<String>) {
    runApplication<SmbApiApplication>(*args)
}

