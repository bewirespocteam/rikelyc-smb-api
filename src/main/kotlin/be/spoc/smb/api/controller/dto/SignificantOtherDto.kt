package be.spoc.smb.api.controller.dto

data class SignificantOtherDto(
        val name: String,
        val surname: String
)
