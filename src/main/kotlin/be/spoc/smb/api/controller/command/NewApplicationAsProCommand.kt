package be.spoc.smb.api.controller.command

import org.hibernate.validator.constraints.Length
import javax.validation.constraints.NotBlank


data class NewApplicationAsProCommand(
        @NotBlank(message = "Surname can not be empty")
        val surName: String,
        @NotBlank(message = "Name can not be empty")
        val name: String,
        @NotBlank(message = "Firm Name can not be empty")
        val firmName: String,
        @NotBlank(message = "Telephone Number can not be empty")
        val telephoneNumber: String,
        @NotBlank(message = "Email can not be empty")
        val email: String,
        @NotBlank(message = "Street can not be empty")
        val street: String,
        @NotBlank(message = "House Number can not be empty")
        val houseNumber: String,
        @NotBlank(message = "City can not be empty")
        val city: String,
        @NotBlank(message = "Postal Code can not be empty")
        val postCode: String,
        @Length(max = 2, message = "Country Code must be a two letter code")
        @NotBlank(message = "Country Code can not be empty")
        val countryCode: String
)