package be.spoc.smb.api.controller.dto

import be.spoc.smb.api.domain.Capacity
import be.spoc.smb.api.domain.MaritalContract
import be.spoc.smb.api.domain.MaritalStatus
import java.time.LocalDate

data class ContactPersonDto(
        val id: String,
        val capacity: Capacity,
        val firmName: String? = null,
        val name: String,
        val surname: String,
        val telephoneNr: String,
        val email: String,
        val placeOfBirth: String? = null,
        val dateOfBirth: LocalDate? = null,
        val maritalStatus: MaritalStatus? = null,
        val martialContract: MaritalContract? = null,
        val significantOther: SignificantOtherDto? = null,
        val street: String,
        val houseNumber: String,
        val city: String,
        val postCode: String,
        val countryCode: String
)
