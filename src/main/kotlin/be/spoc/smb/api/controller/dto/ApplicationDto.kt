package be.spoc.smb.api.controller.dto

data class ApplicationDto (
        val uuid: String,
        val status: String,
        val contactPerson: ContactPersonDto
) { }
