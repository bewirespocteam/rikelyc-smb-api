package be.spoc.smb.api.domain

import java.util.UUID
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "ADDRESS")
data class Address(

        @Id
        @GeneratedValue
        var id: UUID? = null,

        var street: String? = null,

        var houseNumber: String? = null,

        var postCode: String? = null,

        var city: String? = null,

        var countryCode: String? = null
)