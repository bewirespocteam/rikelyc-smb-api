package be.spoc.smb.api.domain

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "MARITAL_STATUS")
data class MaritalStatus(
        @Id
        @GeneratedValue
        var id: UUID? = null,

        var significantOtherName: String? = null,

        var significantOtherSurname: String? = null,

        @Enumerated(EnumType.STRING)
        var status: Status? = null,

        @Enumerated(EnumType.STRING)
        var maritalContract: MaritalContract? = null
) {
    enum class Status {
        SINGLE, MARRIED, LEGAL_COHABITATION
    }

    enum class MaritalContract {
        COMMUNITY_OF_GOODS_CONTRACT,
        COMMUNITY_OF_GOODS_WITHOUT_CONTRACT,
        PURE_SEPARATION_OF_GOODS,
        SEPARATION_OF_GOODS_COMMUNITY_ACQUISITIONS,
        SEPARATION_OF_GOODS_SETTLEMENT,
        OVERALL_COMMUNITY
    }
}