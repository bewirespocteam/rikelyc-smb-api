package be.spoc.smb.api.domain

import java.time.LocalDateTime
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "APPLICATION")
data class Application(

        @Id
        @GeneratedValue
        var id: UUID? = null,

        @Enumerated(EnumType.STRING)
        var status: ApplicationStatus = ApplicationStatus.INITIATED,

        var createdOn: LocalDateTime = LocalDateTime.now(),

        @OneToOne(cascade = [CascadeType.ALL], optional = false, orphanRemoval = true)
        @JoinColumn(name = "contact_person_id")
        var contactPerson: ContactPerson? = null
)