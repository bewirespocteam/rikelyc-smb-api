package be.spoc.smb.api.domain

import java.time.LocalDateTime
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "PERSON")
data class Person(

        @Id
        @GeneratedValue
        var id: UUID? = null,

        var name: String? = null,

        var surname: String? = null,

        var telephoneNumber: String? = null,

        var email: String? = null,

        var placeOfBirth: String? = null,

        var dateOfBirth: LocalDateTime? = null,

        @OneToOne(cascade = [CascadeType.ALL], optional = true, orphanRemoval = true)
        @JoinColumn(name = "address_id")
        var address: Address? = null,

        @OneToOne(cascade = [CascadeType.ALL], optional = true, orphanRemoval = true)
        @JoinColumn(name = "marital_status_id")
        var maritalStatus: MaritalStatus? = null
)