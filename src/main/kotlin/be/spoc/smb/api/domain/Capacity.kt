package be.spoc.smb.api.domain


enum class Capacity {
    FOUNDER,
    PROFESSIONAL
}