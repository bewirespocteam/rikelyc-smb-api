package be.spoc.smb.api.domain

import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "CONTACT_PERSON")
data class ContactPerson(
        @Id
        @GeneratedValue
        var id: UUID? = null,

        @Enumerated(EnumType.STRING)
        var capacity: Capacity? = null,

        var nameOfFirm: String? = null,

        @OneToOne(cascade = [CascadeType.ALL], optional = false, orphanRemoval = true)
        @JoinColumn(name = "person_id")
        var person: Person? = null
)