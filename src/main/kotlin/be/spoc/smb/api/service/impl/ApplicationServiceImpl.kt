package be.spoc.smb.api.service.impl

import be.spoc.smb.api.domain.Application
import be.spoc.smb.api.repository.ApplicationRepository
import be.spoc.smb.api.service.ApplicationService
import org.springframework.stereotype.Service
import java.util.UUID
import javax.persistence.EntityNotFoundException
import javax.transaction.Transactional

@Service
class ApplicationServiceImpl(
        private val repository: ApplicationRepository
) : ApplicationService {
    override fun findById(id: UUID): Application {
        return repository.findById(id)
                .orElseThrow { EntityNotFoundException(id.toString()) }
    }

    @Transactional
    override fun save(application: Application) {
        repository.save(application)
    }
}