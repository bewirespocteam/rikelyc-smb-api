package be.spoc.smb.api.service

import be.spoc.smb.api.domain.Application
import java.util.UUID

interface ApplicationService {
    fun save(application: Application)
    fun findById(id: UUID): Application

}