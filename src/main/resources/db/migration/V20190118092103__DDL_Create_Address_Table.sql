CREATE TABLE ADDRESS
(
  ID           uuid         not null,
  STREET       varchar(255) not null,
  HOUSE_NUMBER varchar(10)  not null,
  POST_CODE    varchar(16)  not null,
  CITY         varchar(255) not null,
  COUNTRY_CODE varchar(2)   not null,
  CONSTRAINT PK_ADDRESS PRIMARY KEY (ID)
)