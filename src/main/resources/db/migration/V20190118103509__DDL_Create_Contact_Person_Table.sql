CREATE TABLE CONTACT_PERSON
(
  ID             uuid        not null,
  CAPACITY       varchar(15) not null,
  NAME_OF_FIRM   varchar(255),
  PERSON_ID      uuid        not null,
  CONSTRAINT PK_CONTACT_PERSON PRIMARY KEY (ID),
  CONSTRAINT FK_CONTACT_PERSON_PERSON FOREIGN KEY (PERSON_ID) REFERENCES PERSON (ID),
  CONSTRAINT CK_VALID_CAPACITY CHECK (
      (CAPACITY = 'FOUNDER' AND NAME_OF_FIRM IS NULL) OR
      (CAPACITY = 'PROFESSIONAL' AND NAME_OF_FIRM IS NOT NULL)
    )
)